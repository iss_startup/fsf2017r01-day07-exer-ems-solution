// TODO: Create a search page which displays dummy data (list of employees) and allows users to search
// TODO: (using filter) these data
// TODO: 5. Define a controller for search.html. Name the controller SearchCtrl
// TODO: 6. Create dummy data to be displayed when users enter search.html
// TODO: 6. Dummy data should be an array of employee objects; should have No, First and Last name, Phone Number
// Always use an IIFE, i.e., (function() {})();
(function () {
    // TODO: 5.2 Attach your controller to the DMS module
    // Attaches a SearchCtrl to the DMS module
    angular
        .module("EMS")
        .controller("SearchCtrl", SearchCtrl);

    // TODO: 5.3 Inject dependencies
    // Dependency injection. An empty [] means SearchCtrl does not have dependencies
    SearchCtrl.$inject = [];

    // TODO: 5.1 Define controller. Call it SearchCtrl
    // Search function declaration
    function SearchCtrl() {
        // TODO: 6.1 Assign this to vm variable

        // Declares the var vm (for ViewModel) and assigns it the object this (in this case, the SearchCtrl). Any
        // function or variable that you attach to vm will be exposed to callers of SearchCtrl, e.g., search.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // TODO: 6.2 Initialize an array of objects with properties empNo, empFirstName, empLastName, empPhoneNumber
        // TODO: 6.2 with some dummy data. For example, empNo: 1020, empFirstName: Emily
        vm.employees = [
            {
                empNo: 1001,
                empFirstName: 'Emily',
                empLastName: 'Smith',
                empPhoneNumber: '6516 2093'
            }
            , {
                empNo: 1002,
                empFirstName: 'Varsha',
                empLastName: 'Jansen',
                empPhoneNumber: '6516 2093'
            }
            ,
            {
                empNo: 1003,
                empFirstName: 'Julie',
                empLastName: 'Black',
                empPhoneNumber: '6516 2093'
            }
            , {
                empNo: 1004,
                empFirstName: 'Fara',
                empLastName: 'Johnson',
                empPhoneNumber: '6516 2093'
            }
            ,
            {
                empNo: 1005,
                empFirstName: 'Justin',
                empLastName: 'Zhang',
                empPhoneNumber: '6516 2093'
            }
            , {
                empNo: 1006,
                empFirstName: 'Kenneth',
                empLastName: 'Black',
                empPhoneNumber: '6516 2093'
            }

        ];
    }
})();